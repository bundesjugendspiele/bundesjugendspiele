# Bundesjugendspiele

## Start in production mode

```bash
# Clone just the base repository, all images will be pulled automatically
git clone git@git.thm.de:bundesjugendspiele/bundesjugendspiele.git
# Go into the cloned directory
cd bundesjugendspiele
# Start the docker stack in production mode
docker-compose -f docker-compose.yml -f docker-compose.prod.yml up
# (Optional) add some test data
docker exec -it  bundesjugendspiele_backend npm run seed:run
```

Just visit [http://localhost:8000/](http://localhost:8000/) to see the application.

## Start in development mode

```bash
# Clone the repository and all their submodules
git clone --recursive git@git.thm.de:bundesjugendspiele/bundesjugendspiele.git
# Go into the cloned directory
cd bundesjugendspiele
# Start the docker stack in development mode
docker-compose -f docker-compose.yml -f docker-compose.dev.yml up
# (Optional) add some test data
docker exec -it  bundesjugendspiele_backend_dev npm run seed:run
```

If you wanna rebuild the images, just use the `--build` flag at the end.

## About the application

This app contains some relly good key features. The database itself is isolated from the frontend and only accessible from the backend service.\
The backend service itself is accessible through a REST api, which makes it easier to use with frontend applications like VueJS. VueJS is used in frontend and can eassily communicate with the backend. To bind the entire network traffic, an nginx with a reverse proxy is used.

One of the main features of our app is the clean and well seperated components between frontend and backend.\
Also the aspect that this app is completly free and open source makes it perfect for projects like the "Bundesjugendspiele".\
The project itself is licensed under the [EUPL](https://git.thm.de/bundesjugendspiele/bundesjugendspiele/-/blob/main/LICENSE) which makes it safe to use in the EU, without worrying to break laws.\
Optimization and speed is on of our key features, every request is paginated. You can fetch and limit your data, which makes it extremly fast and compact. Perfect for mobile applications. The data itself can be sorted or be searched, just add a query and sort/filter your data backendly.

### Web locations

Applications are accessible under:

- Adminer [http://localhost:8000/adminer](http://localhost:8000/adminer)
- Backend [http://localhost:8000/api](http://localhost:8000/api)
- Frontend [http://localhost:8000/](http://localhost:8000/)
- Swagger [http://localhost:8000/swagger](http://localhost:8000/swagger)

### Authors

- Daniél Kerkmann
- Maik Mursall

### License

The code is licensed under the [EUPL 1.2 or higher](LICENSE).
